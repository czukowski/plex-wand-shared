Wand connector for Plex Channels
================================

[Wand-py] is a [ctypes]-based simple [ImageMagick] binding for Python.

[Plex] is a personal media server capable of playing remote content from the internet using [channels], that
are written in Python. But Plex comes with its own bundled Python binaries and it's not that simple to install
any 3rd party libraries to it.

This small piece of code provides way to use ImageMagick in a Plex channel via Wand-py library, as simple as
adding a few git-submodules to your own channel. Right, I assume you use [git] as a version control for your
channel development (possibly silly of me, but if you don't, then you don't really need this connector).

This is not a plug-and-play solution though, because ImageMagick binaries still have to be installed to the
system manually.

Installation
------------

  1. Install ImageMagick

     Navigate to [ImageMagick] website and download binaries that match your OS (for Windows, use x86 dll binaries,
     since Python bundled with Plex may have difficulties loading 64bit dlls).

     Some ImageMagick setup directions can be found in the Wand docs's
     [Installation section](http://docs.wand-py.org/en/0.3.7/guide/install.html).

     Facing `WindowsError` code 126 and ImageMagick is correctly installed? Wand is trying to load MS Visual C++
     Runtime DLL that was used to compile python, see if the corresponding redistributables are correctly installed
     too (see `wand/api.py`, near end of file). 

  2. Add Wand-py repo to your channel:

     Go to (create if needed) the `Libraries/Shared` folder in your `Contents` directory (this is where custom
     libraries can be placed, that would be free from the sandbox restrictions applied to the rest of the channel
     code by Plex Framework) and add a submodule, for example, like this:

        git submodule add https://github.com/dahlia/wand.git wand

  3. Add this repo to your channel:

     Go to (create if needed) the `Shared Code` folder in your `Contents` directory and add a submodule:

        git submodule add https://bitbucket.org/czukowski/plex-wand-shared.git wand_plex

  4. Create your own shared code file, where you'll be doing your image processing, for example `imaging.pys`,
     ie your shared code will be called __imaging__.

     Note: don't call it __wand__ or __wand_plex__, or it'll conflict with the submodules you've just added.

  5. Edit your channel's `Info.plist` and add the following lines to tell Plex to allow your channel to use
     a custom library.
     
        <key>PlexPluginCodePolicy</key>
	    <string>Elevated</string>

Usage
-----

Some sample code follows:

  1. Add a method to your channel's controller core (usually `Code/__init__py`):

        :::python
            # Generates a movie thumbnail image
            @route(PREFIX+'/thumbnails/{key}')
            def MovieThumbnail(key):
                try:
                    image = SharedCodeService.imaging.logo(GetMovieThumbnail(key), ICON)
                    Response.Headers['Content-Disposition'] = 'inline;filename="%s.%s"' % (key, image.format.lower())
                    return DataObject(image.make_blob(image.format), image.mimetype)
                except ImportError:
                    return R(ICON)

     where `GetMovieThumbnail` is a hypothetical function that loads a movie web page and returns its cover image.
     In case there's a problem with ImageMagick installation, `ImportError` is raised, so you'll probably want to
     return some default thumbnail if this happens, as shown above.

     The line `SharedCodeService.imaging.logo` calls `logo` function in your __imaging__ shared code service.

     Note the `DataObject` class, that'll allow you to specify the content-type of your data. Otherwise, it would
     be automatically sent as 'text/plain'.

  2. Make sure your `imaging.pys` file contains the following import statement:

        :::python
            import wand_plex

     to load the connector code and the `logo` function:

        :::python
            def logo(thumbnail, icon):
                image = wand_plex.load_from_url(thumbnail, sleep=0.1, cacheTime=3600)
                image.resize(100, 160)
                logo = wand_plex.load_from_resource(icon)
                logo.resize(30, 30)
                image.composite(image=logo, top=120, left=60)
                return image

     Note the `sleep` parameter, use it to prevent __503 Service Unavailable__ errors from the server if you load
     several images in a row. Also using `cacheTime` may be useful to further reduce the server load.

  3. Change the `thumb` parameter of your DirectoryObjects to a callback to your new `MovieThumbnail` function:

        :::python
            ...
            oc.add(DirectoryObject(
                key=Callback(PlayMovie, category=key),
                title=movie_title,
                thumb=Callback(MovieThumbnail, key=key)
            ))
            ...

Note that this is just a general idea of usage.

  * Another example, that combines up to 5 movie covers into one image:

        :::python
            def combine(thumbnails):
                if len(thumbnails) > 5:
                    thumbnails = thumbnails[:5]
                images = map(lambda image_url: wand_plex.load_from_url(image_url), thumbnails)
                composite_image = images[0]
                if len(thumbnails) > 1:
                    step = composite_image.width / len(thumbnails)
                    for i, image in enumerate(images[1:]):
                        image.resize(composite_image.width, composite_image.height)
                        composite_image.composite(image=image, top=0, left=(step * i + step))
                return composite_image


  [channels]: https://plexapp.zendesk.com/hc/en-us/categories/200109616-Channels
  [ctypes]: http://docs.python.org/library/ctypes.html#module-ctypes
  [git]: http://git-scm.com/
  [ImageMagick]: http://www.imagemagick.org/
  [Plex]: https://plex.tv/
  [Wand-py]: http://wand-py.org/